import { ReturnStatement } from '@angular/compiler';
import { Injectable } from '@angular/core';

import { listProducts } from '../models/ListProducts';
import { Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent } from 'rxjs';

//Author of Service: Shubham Anand

@Injectable({
  providedIn: 'root'
})
export class ProductHandlingService {

  products: listProducts[] = [];
  private subjectData=new Subject<any>();


  //Array of objects of Product type for each item and its rate
  ProductList:listProducts[]=[{
    Name:"Muesli kellogs",
    Rate:315
  },{
    Name:"Dairy Milk",
    Rate:35
  },
  {
    Name:"Rice",
    Rate:110
  },
  {
    Name:"Soya Bean",
    Rate:110
  },
  {
    Name:"Sugar",
    Rate:40
  },
  {
    Name:"Chana Daal",
    Rate:52
  },{
    Name:"Muesli",
    Rate:315
  },
  {
    Name:"Hair Oil",
    Rate:50
  },
{
  Name:"Juice",
  Rate:105
},
{
  Name:"Maggie",
  Rate:12
},
{
  Name:"Marie Gold",
  Rate:22
},
{
  Name:"Ashirwaad Aata",
  Rate:135
}];

constructor() { }

      //It will add to list of products on each item click for add to cart.
      store(value:listProducts){
        this.products.push(value);
      }

      // This method will search the rate of each selected item from the array of objects created above
      Search(value:String):number{
      let index = this.ProductList.findIndex(pr => pr.Name === value);
      return this.ProductList[index].Rate;
      }

      //For testing purpose of execuion of code , to show the selected items.
      show(){
        console.log(this.products);
      }
      //It will add to list of products on each item click for add to cart to subject.
      update(value:listProducts[]){
        this.subjectData.next({value});
      }

      //It will return the list of selected item, which will further used to store in observable
      ReturnList():listProducts[]{
        return this.products;
      }

      //Method to store the array of product class type in observable
      fetch():Observable<any>{

        return this.subjectData.asObservable();
}
}
