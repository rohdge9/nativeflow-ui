import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/User';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginServicesService {
  user:User;
url = "http://localhost:8080/user-auth";
constructor(private http: HttpClient) {
  this.user = new User();
}

checkData(data : User) : Observable<Object> {
  return this.http.post(this.url, data);
}

  storeUser(modelUser:User){
    this.user = modelUser;
  }

  retrieveUser(){
  return this.user;
  }
}
