import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dynamic-payment-option',
  templateUrl: './dynamic-payment-option.component.html',
  styleUrls: ['./dynamic-payment-option.component.css']
})
export class DynamicPaymentOptionComponent implements OnInit {
  paymentOptions:any = {};
  showCardFillOption = false;
  typeOfPaymentSelected :any;
  constructor() {
    this.paymentOptions={
      'status':'success',
      'txn_id':'random_string',
      'cardDetails':[
        'credit',
        'debit'
      ]
    };
  }


  ngOnInit(): void {
  }

  onClick(value:any) {
    console.log(value);
    this.typeOfPaymentSelected = value;
    if (value.toString()=="credit"){
      this.showCardFillOption = true;
    }
  }
}
