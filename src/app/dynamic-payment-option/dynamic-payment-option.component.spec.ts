import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DynamicPaymentOptionComponent } from './dynamic-payment-option.component';

describe('DynamicPaymentOptionComponent', () => {
  let component: DynamicPaymentOptionComponent;
  let fixture: ComponentFixture<DynamicPaymentOptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DynamicPaymentOptionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicPaymentOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
