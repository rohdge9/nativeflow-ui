import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {LoginPageComponent} from "./login-page/login-page.component";
import {FormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {AppRoutingModule} from "./app-routing.module";
import {HttpClientModule} from "@angular/common/http";
import {DisplayProductComponent} from "./display-products/display-product.component";
import {CartTableComponent} from "./cart-table/cart-table.component";
import {RouterModule, Routes} from "@angular/router";
import { DynamicPaymentOptionComponent } from './dynamic-payment-option/dynamic-payment-option.component';
import { FailedPaymentComponent } from './failed-payment/failed-payment.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    DisplayProductComponent,
    CartTableComponent,
    DynamicPaymentOptionComponent,
    FailedPaymentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule
  ],
  providers: [],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
