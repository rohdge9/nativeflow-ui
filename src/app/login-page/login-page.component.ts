import { LoginServicesService } from '../../services/loginServices.service';
import { Component, OnInit } from '@angular/core';
import { User } from '../../models/User';
import { Observable } from 'rxjs';
import { NgForm } from '@angular/forms';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  result : any;
  modelUser:User;
  constructor(private services : LoginServicesService,private router:Router) {
    this.modelUser = new User();
  }

  ngOnInit() {
  }

  onSubmit() {
    // this.services.checkData(data).subscribe((response: any) => this.result = response);
    let preprocessData :any= {};
    preprocessData['userName'] = this.modelUser.userId;
    preprocessData['password'] = this.modelUser.password;
    console.log(preprocessData);
    this.services.storeUser(this.modelUser);
    this.router.navigate(['/select-products']);
  }

}
