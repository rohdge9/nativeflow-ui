import { listProducts } from '../../models/ListProducts';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Observable } from 'rxjs';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ProductHandlingService } from '../../services/ProductHandling.service';
import {connectableObservableDescriptor} from "rxjs/internal/observable/ConnectableObservable";
import {map} from "rxjs/operators";
import {LoginServicesService} from "../../services/loginServices.service";
//Author: Shubham Anand
//Author of this Componenet: Shubham Anand
@Component({
  selector: 'app-CartTable',
  templateUrl: './cart-table.component.html',
  styleUrls: ['./cart-table.component.css']
})
export class CartTableComponent implements OnInit {
  //private subname:Subscription;
  //list to store the objects of class product which will contain selected items with rate
  ItemsList:listProducts[]=[];
  sum: number = 0;
  //observable will store the list of selected items
  observable=this.ser.ReturnList();
  constructor(
    private route: ActivatedRoute, private ser:ProductHandlingService,private loginService:LoginServicesService
  ) {

  }

  //it will sum the amount of total money of all items selected for cart
  ngOnInit() {
    this.observable.forEach(a => this.sum += a.Rate);
    let listProductsS:any=[];
    let preprocessedRequest:any={};
    this.observable.forEach((element)=>{
      let tempVar= {
        'name':element.Name,
        'rate':element.Rate
      };
      listProductsS.push(tempVar);
    });
    preprocessedRequest['amountTotal']=this.sum,
    preprocessedRequest['cart'] = listProductsS;
    preprocessedRequest['userName'] = this.loginService.retrieveUser().userId;
    console.log(preprocessedRequest);
  }

}
